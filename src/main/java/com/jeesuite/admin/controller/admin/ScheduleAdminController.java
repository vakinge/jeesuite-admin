package com.jeesuite.admin.controller.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.catalina.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesuite.admin.dao.entity.MonitorServerEntity;
import com.jeesuite.admin.dao.mapper.MonitorServerEntityMapper;
import com.jeesuite.admin.model.SelectOption;
import com.jeesuite.admin.model.WrapperResponseEntity;
import com.jeesuite.common.JeesuiteBaseException;
import com.jeesuite.scheduler.model.JobGroupInfo;
import com.jeesuite.scheduler.monitor.MonitorCommond;
import com.jeesuite.scheduler.monitor.SchedulerMonitor;

@RequestMapping("/admin/schedule")
@Controller
public class ScheduleAdminController {

	private static Map<String, SchedulerMonitor> monitorInstances = new HashMap<>();
	
	private @Autowired MonitorServerEntityMapper monitorServerMapper;
	
	private SchedulerMonitor getSchedulerMonitor(String profile){
		SchedulerMonitor monitor = monitorInstances.get(profile);
		if(monitor == null){
			synchronized (monitorInstances) {
				MonitorServerEntity monitorServer = monitorServerMapper.findByEnvAndMoudule(profile, "scheduler");
				if(monitorServer == null) throw new JeesuiteBaseException(2001, "无["+profile+"][schedule]配置"); 
				monitor = new SchedulerMonitor("zookeeper", monitorServer.getServers());
				monitorInstances.put("dev", monitor);
			}
		}
		
		return monitor;
	}
	
	@RequestMapping(value = "group/{env}", method = RequestMethod.GET)
	public @ResponseBody List<SelectOption> getGroups(@PathVariable("env") String env){
		
		List<SelectOption> result = new ArrayList<>();
		SchedulerMonitor monitor = getSchedulerMonitor(env);
		List<String> groups = monitor.getGroups();
		if(groups != null){
			for (String g : groups) {
				result.add(new SelectOption(g,g));
			}
		}
		return result;
	}
	
	@RequestMapping(value = "clear_invalid_group/{env}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<WrapperResponseEntity> clearInvalidGroup(@PathVariable("env") String env){
		
		SchedulerMonitor monitor = getSchedulerMonitor(env);
		if(monitor != null )monitor.clearInvalidGroup();
		
		return new ResponseEntity<WrapperResponseEntity>(new WrapperResponseEntity(true),HttpStatus.OK);
	}
	
	@RequestMapping(value = "group/jobs", method = RequestMethod.POST)
	public ResponseEntity<WrapperResponseEntity> listGroupAllJobs(@RequestBody Map<String, String> params){
		String env = params.get("env");
		String groupName = params.get("groupName");
		JobGroupInfo groupInfo = getSchedulerMonitor(env).getJobGroupInfo(groupName);
		return new ResponseEntity<WrapperResponseEntity>(new WrapperResponseEntity(groupInfo),HttpStatus.OK);
	}
	
	@RequestMapping(value = "job/{cmd}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<WrapperResponseEntity> sendCommond(@PathVariable("cmd") String cmd,@RequestBody Map<String, String> params){
		
		String env = params.get("env");
		String group = params.get("group");
		String job = params.get("job");
		String data = params.get("data");
		
		MonitorCommond monitorCmd = null;
		if("exec".equals(cmd)){
			monitorCmd = new MonitorCommond(MonitorCommond.TYPE_EXEC, group, job, null);
		}else if("switch".equals(cmd)){
			monitorCmd = new MonitorCommond(MonitorCommond.TYPE_STATUS_MOD, group, job, data);
		}else if("updatecron".equals(cmd)){
			monitorCmd = new MonitorCommond(MonitorCommond.TYPE_CRON_MOD, group, job, data);
		}
		
		WrapperResponseEntity rspEntity = null;
		if(monitorCmd != null){
			try {	
				getSchedulerMonitor(env).publishEvent(monitorCmd);
			    rspEntity = new WrapperResponseEntity(200, "发送执行事件成功");
			} catch (Exception e) {
				rspEntity = new WrapperResponseEntity(500, "publish Event发生错误");
			}
		}else{
			rspEntity = new WrapperResponseEntity(500, "未知指令："+cmd);
		}
		
		return new ResponseEntity<WrapperResponseEntity>(rspEntity,HttpStatus.OK);
	}
}
