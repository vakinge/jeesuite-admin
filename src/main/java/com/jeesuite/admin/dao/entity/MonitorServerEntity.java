package com.jeesuite.admin.dao.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jeesuite.mybatis.core.BaseEntity;

@Table(name = "monitor_servers")
public class MonitorServerEntity extends BaseEntity {
	
	public static enum MonitorServerType {

		kafka("kafka地址"),kafka_zookeeper("kafka注册Zookeeper地址"),scheduler("定时任务注册中心地址");
		private final String desc;

		private MonitorServerType(String desc) {
			this.desc = desc;
		}

		public String getDesc() {
			return desc;
		}

		public String getName() {
			return name().replaceAll("\\_", ":");
		}
		
	}
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String moudule;

    private String env;

    private String servers;

    private Boolean enabled;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return moudule
     */
    public String getMoudule() {
        return moudule;
    }

    /**
     * @param moudule
     */
    public void setMoudule(String moudule) {
        this.moudule = moudule;
    }

    /**
     * @return env
     */
    public String getEnv() {
        return env;
    }

    /**
     * @param env
     */
    public void setEnv(String env) {
        this.env = env;
    }

    /**
     * @return servers
     */
    public String getServers() {
        return servers;
    }

    /**
     * @param servers
     */
    public void setServers(String servers) {
        this.servers = servers;
    }

    /**
     * @return enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    public String getServerType(){
    	return MonitorServerType.valueOf(moudule.replaceAll("\\:", "_")).desc;
    }
}