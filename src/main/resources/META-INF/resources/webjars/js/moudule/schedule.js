layui.use(['layer', 'laytpl', 'form'],function() {
    var $ = layui.jquery,
    laytpl = layui.laytpl,
    form = layui.form();
    //
    form.on('submit(search)',function(data) {
        var loading = layer.load();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '../admin/schedule/group/jobs',
            contentType: "application/json",
            data: JSON.stringify(data.field),
            complete: function() {
                layer.close(loading);
            },
            success: function(data) {
                if (data.code == 200) {
                    var tpl = $('#task_list_tpl').html();
                    laytpl(tpl).render(data,
                    function(html) {
                        $("#content").html(html);
                    });
                    tpl = $('#node_list_tpl').html();
                    laytpl(tpl).render(data,
                    function(html) {
                        $("#node_content").html(html);
                    });
                } else {
                    layer.msg(data.msg, {
                        icon: 5
                    });
                }
            },
            error: function(xhr, type) {
                layer.msg('系统错误', {
                    icon: 5
                });
            }
        });
        return false;
    });
    //
    form.on('select(env_select)',function(data) {
        var dataurl = '../admin/schedule/group/' + data.value;
        $.getJSON(dataurl,
        function(result) {
        	if(result.code && result.code != 200){
        		layer.msg(result.msg, { icon: 5});
        		return;
        	}
            var opthtml;
            for (var index in result) {
                opthtml = '<option value="' + result[index].value + '">' + result[index].text + '</option>';
                $('#group_select').append(opthtml);
            }
            form.render('select');
        });
    });
    
    $("#content").on('click', ".J_commond_btn",function() {
    	var cmd = $(this).attr('data-cmd'),dataref = $(this).attr('data-ref');
    	var  params = {};
    	params.env = $('#env_select').val();
    	params.group = $('#group_select').val();
    	params.job = $(this).attr('data-id');
    	
    	var data = dataref ? $(dataref).val() : $(this).attr('data');
    	if(data)params.data = data;
    	
    	layer.confirm('确认执行该操作么？', {
		    btn: ['确定','取消'], //按钮
		    shade: false //不显示遮罩
		}, function(index){
			$.ajax({
	            dataType: "json",
	            type: "POST",
	            url: '../admin/schedule/job/'+cmd,
	            contentType: "application/json",
	            data: JSON.stringify(params),
	            complete: function() {
	                layer.close(index); 
	            },
	            success: function(data) {
	                if (data.code == 200) {
	                	layer.msg(data.msg  || '操作成功', {icon: 6});
	                	//setTimeout(function(){window.location.reload();},500);
	                	$('.J_search').click();
	                }else{
	                	layer.msg(data.msg || '操作失败' , {icon: 5});
	                }
	            },
	            error: function(xhr, type) {
	                layer.msg('系统错误', {
	                    icon: 5
	                });
	            }
	        });
		}, function(index){
	    	layer.close(index); 
		});
        
    });
    
    $(".J_clear").on('click',function(){
    	var env = $('#env_select').val();
    	if(env == ''){layer.msg('请选择环境' , {icon: 5});return;}
    	$.getJSON('../admin/schedule/clear_invalid_group/'+env,function(data){
            if (data.code == 200) {
            	layer.msg(data.msg  || '操作成功', {icon: 6});
            }else{
            	layer.msg(data.msg || '操作失败' , {icon: 5});
            }
        });
    });

});